package com.xmltojson.com;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

@SpringBootApplication
public class XmLtoJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmLtoJsonApplication.class, args);
		Logger logger = Logger.getLogger(String.valueOf(XmLtoJsonApplication.class));
		logger.info("********************Debut Conversion **********************");

		try {
				// Create an instance of XmlMapper
				XmlMapper xmlMapper = new XmlMapper();

				// Read the XML file and convert it to a JsonNode
				File xmlFile = new File("src/main/resources/files/xmlFile.xml");
				JsonNode jsonNode = xmlMapper.readTree(xmlFile);

				// Create an instance of ObjectMapper for JSON conversion
				ObjectMapper jsonMapper = new ObjectMapper();

				// Convert the JsonNode to a JSON string
				String jsonString = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);

				// Print the JSON string to console or write to file
				logger.info(jsonString);
				// Optionally, write the JSON string to a file
				File jsonFile = new File("src/main/resources/files/jsonFile.json");
				jsonMapper.writeValue(jsonFile, jsonNode);
			logger.info("********************Fin Conversion**********************");
		} catch (Exception ex) {
			throw  new RuntimeException("Erreur d'ecriture du fichier en format json");
		}
	}

}
